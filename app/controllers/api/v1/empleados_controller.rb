class Api::V1::EmpleadosController < ApplicationController
  #GET /empleados
  def index
    @empleados = Empleado.all
    render json: @empleados, :except => [:created_at, :updated_at]
  end

  #Get /empleado/:id
  def show
    @empleado = Empleado.find(params[:id])
    render json: @empleado
  end

  #Post /empleados
  def create
    @empleado = Empleado.new(empleado_params)
    if @empleado.save
      render json: @empleado
    else
      render error: {error: 'No se pudo crear el empleado' }, status: 400
    end
  end

  #Put /empleados/:id
  def update
    @empleado = Empleado.find(params[:id])
    if @empleado
      @empleado.update(empleado_params)
      render json: { message: 'Empleado actualizado exitosamente' }, status:200
    else
      render json: { error: 'No se pudo actualizar el empleado' }, status:400
    end
  end

  #Eliminar el empleado
  def destroy
    @empleado = Empleado.find(params[:id])
    if @empleado
      @empleado.destroy
      render json: { message: 'Empleado eliminado exitosamente.' }, status: 200
    else
      render json: { error: 'No se pudo elminar el empleado' }, status: 400
    end
  end

  private
  def empleado_params
    params.require(:empleado).permit(:area, :nombre, :apellido_paterno, :apellido_materno, :telefono, :fecha_nacimiento, :email)
  end

end
