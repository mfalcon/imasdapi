# README

Como iniciar el proyecto

Things you may want to cover:

Instalar lo siguiente:
Ruby vesión 2.5
Rails version 5.1.7

Utiliza Mysql como motor de base de datos

Para correr el proyecto con acceso a ip local utiliza
 rails server -b 0.0.0.0

Urls para las probar las API

GET ALL
api/v1/empleados

GET ONE
api/v1/empleados/{id}

DELETE ONE
api/v1/empleados/{id}

PATCH
api/v1/empleados/{id}


Los datos se pasan como un json para el PATCH, POST
utiliza como puerto 3000

BASE URL  http://{ip}:3000/


#Base de Datos
Para la base de datos los parametros de configuracion esta en la carpeta config archivo database.yml

Con los siguientes datos:
user: root
passowrd: devtest
