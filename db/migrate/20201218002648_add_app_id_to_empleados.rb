class AddAppIdToEmpleados < ActiveRecord::Migration[5.1]
  def change
    add_column :empleados, :app_id, :int
  end
end
