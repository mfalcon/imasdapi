class CreateEmpleados < ActiveRecord::Migration[5.1]
  def change
    create_table :empleados do |t|
      t.string :area
      t.string :nombre
      t.string :apellido_paterno
      t.string :apellido_materno
      t.string :telefono
      t.string :fecha_nacimiento
      t.string :email

      t.timestamps
    end
  end
end
