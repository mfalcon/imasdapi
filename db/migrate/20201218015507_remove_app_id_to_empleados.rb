class RemoveAppIdToEmpleados < ActiveRecord::Migration[5.1]
  def change
    remove_column :empleados, :app_id, :int
  end
end
